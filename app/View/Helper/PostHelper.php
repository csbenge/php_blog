<?php

/**
 * Post View Helper
 */

App::uses('AppHelper', 'View/Helper');
App::import("Model", "User"); 
App::import("Model", "Post");  
App::import("Model", "Comment");   

class PostHelper extends AppHelper {
    public $helpers = array('Html');

	public function getUserName($id = null) {
		$user_model = new User();  
		$this_user = $user_model->find('first', array('conditions'=>array('User.id'=>$id)));
    return $this_user['User']['username'];
  }

  public function getCommentCount($id = null) {
		$comment_model = new Comment();
		$rowcount = $comment_model->find('count', array('conditions' => array('Comment.post_id' => $id)));
    return $rowcount;
  }

  public function thisUserWroteComment($id = null) {
		$comment_model = new Comment();
		$this_comment = $comment_model->find('first', array('conditions'=>array('Comment.id'=>$id)));

		if (CakeSession::read('Auth.User.id') === $this_comment['Comment']['user_id']) {
			return true;
		} else {
    	return false;
		}
  }

}
