<?php

/* /app/View/Helper/ButtonHelper.php (using other helpers) */

App::uses('AppHelper', 'View/Helper');

class ButtonHelper extends AppHelper {
  public $helpers = array('Html', 'Form');

  public function addButton($name) {
    $button = $this->Html->link($name, array('action' => 'add'), array('class' => 'btn btn-small btn-primary'));
    return $button;
  }
  
  public function addButtonPath($name, $path) {
    $button = $this->Html->link($name, $path, array('class' => 'btn btn-small btn-primary'));
    return $button;
  }
  
  public function editButton($url) {
    $button = $this->Html->link(__("Edit"), array("action" => "edit", $url), array("class" => "btn btn-mini"));
    return $button;
  }
  
  public function deleteButton($url, $name) {
    $button = $this->Form->postLink(__('Delete'), array('action' => 'delete', $url), array('class' => 'btn btn-mini'), __('Are you sure you want to delete "%s"?', $name));
    return $button;
  }
  
  public function deleteButtonAction($action, $url, $name) {
    $button = $this->Form->postLink(__('Delete'), array('action' => $action, $url), array('class' => 'btn btn-mini'), __('Are you sure you want to delete "%s"?', $name));
    return $button;
  }
  
  public function removeButton($url, $name) {
    $button = $this->Form->postLink(__('Remove'), array('action' => 'delete', $url), array('class' => 'btn btn-mini'), __('Are you sure you want to remove "%s"?', $name));
    return $button;
  }
  
  public function removeButtonGroupMember($url, $group_id, $name) {
    $button = $this->Form->postLink(__('Remove'), array('controller' => 'Groupmembers', 'action' => 'delete', $url, $group_id), array('class' => 'btn btn-mini'), __('Are you sure you want to remove "%s"?', $name));
    return $button;
  }
}