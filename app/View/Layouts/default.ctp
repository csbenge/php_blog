<!-- File: /app/View/Layouts/default.ctp -->

<!-- Enable AppDynamcis EUEM -->

<script>window['adrum-start-time'] = new Date().getTime();</script><script src="js/adrum.js"></script>

<script type="text/javascript">$(document).foundation();</script>

<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'iBlog: My Blog!');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>

<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('foundation');
    echo $this->Html->css('foundation-icons');

		echo $this->Html->script('vendor/jquery.js');
		echo $this->Html->script('foundation/foundation.js');
    echo $this->Html->script('foundation/foundation.alert.js');
		echo $this->Html->script('foundation/foundation.accordion.js');
		
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>

<!-- BEGIN: TOP NAV BAR -->

<div class="contain-to-grid sticky">
	<nav class="top-bar" data-topbar role="navigation">
  <!-- Title -->
  <ul class="title-area">
    <li class="name"><h1><a href="/blog">iBlog</a></h1></li>

    <!-- Mobile Menu Toggle -->
    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
  </ul>

  <!-- Top Bar Section -->
  <section class="top-bar-section">

    <!-- Top Bar Left Nav Elements -->
    <ul class="left">

      <!-- Search | has-form wrapper -->
      <li class="has-form">
        <div class="row collapse">
          <div class="large-8 small-9 columns">
            <input type="text" placeholder="Find Stuff">
          </div>
          <div class="large-4 small-3 columns">
            <a href="#" class="alert button expand">Search</a>
          </div>
        </div>
      </li>
      <li class="has-form">
        <a href="/blog/posts" class="button">Posts</a>

      </li>
      
      <?php if (CakeSession::read('Auth.User.role') === 'admin'): ?>
      <li class="has-form">
        <a href="/blog/users" class="button">Users</a>
      </li>
      <?php endif ?>
    </ul>

    <!-- Top Bar Right Nav Elements -->
    <ul class="right">
      <!-- Divider -->
      <li class="divider"></li>

      <!-- Dropdown -->
      <li class="has-dropdown"><a href="#">Dropdown</a>
        <ul class="dropdown">
          <li><label>Level One</label></li>
          <li><a href="#">Sub-item 1</a></li>
          <li><a href="#">Sub-item 2</a></li>
          <li class="divider"></li>
          <li><a href="#">Sub-item 3</a></li>
          <li class="has-dropdown"><a href="#">Sub-item 4</a>

            <!-- Nested Dropdown -->
            <ul class="dropdown">
              <li><label>Level Two</label></li>
              <li><a href="#">Sub-item 2</a></li>
              <li><a href="#">Sub-item 3</a></li>
              <li><a href="#">Sub-item 4</a></li>
            </ul>
          </li>
          <li><a href="#">Sub-item 5</a></li>
        </ul>
      </li>

      <li class="divider"></li>

      <!-- Anchor -->
      <li><a href="#">Button</a></li>
      <li class="divider"></li>

      <!-- BEGIN: Login/Logout -->
      <?php if (CakeSession::read('Auth.User.username')): ?>
        <li class="has-form show-for-large-up">
          <a href="/blog/users/logout" class="button">Logout</a>
        </li>
        <li class="name"><h1>
          <span style="font-size: 0.525em;color:white;">
            <?php echo CakeSession::read('Auth.User.username'); ?>
          </span>
        </li>
      <?php else: ?>
        <li class="has-form show-for-large-up">
          <a href="/blog/users/login" class="button">Login</a>
        </li>
      <?php endif ?>
      <!-- END: Login/Logout -->
      
    </ul>
  </section>
	</nav>
</div>	

<!-- END: TOP NAV BAR -->

<!-- MAIN CONTAINER -->

<div class="row" id="container">

	<div class="row" id="messages">
		<div class="large-12 columns">

			<!-- MESSAGES -->

      <?php 
        if ($this->Session->check('Message.auth')) {
          echo '<div class="row">
                  <div data-alert class="alert-box success round" style="text-align: center;font-size: 1.25em">';
          echo $this->Session->flash('success');
          echo '<a href="#" class="close">&times;</a>
              </div>
            </div>';
        }
      ?>
			<?php 
				if ($this->Session->check('Message.success')) {
					echo '<div class="row">
                  <div data-alert class="alert-box success round" style="text-align: center;font-size: 1.25em">';
          echo $this->Session->flash('success');
          echo '<a href="#" class="close">&times;</a>
              </div>
            </div>';
				}
			?>
			<?php 
				if ($this->Session->check('Message.warning')) {
					echo '<div class="row">
                  <div data-alert class="alert-box warning round" style="text-align: center;font-size: 1.25em">';
          echo $this->Session->flash('warning');
          echo '<a href="#" class="close">&times;</a>
              </div>
            </div>';
				}
			?>
      <?php 
        if ($this->Session->check('Message.alert')) {
          echo '<div class="row">
                  <div data-alert class="alert-box alert round" style="text-align: center;font-size: 1.25em">';
          echo $this->Session->flash('alert');
          echo '<a href="#" class="close">&times;</a>
              </div>
            </div>';
        }
      ?>

    </div>
  </div>

  <!-- CONTENT -->

	<div class="row" id="content">
    <div class="large-12 columns">
      <?php echo $this->fetch('content'); ?>
    </div>
  </div>
		
  
  <!-- FOOTER -->

	<div class="row" id="footer">
	</div>

  <!-- DATABASE -->
  <!--
  <div class="row" id="database">
  	<hr>
  	<h4>Database</h4>
  	<?php echo $this->element('sql_dump'); ?>
  </div>
  -->

</div>

</body>
</html>