<!-- File: /app/View/Posts/view.ctp -->

<div class="row">

	<div class="row">
		<div class="large-12 columns">

		<fieldset>
	    <legend>
	    	<span style="font-size: 1.75em;font-weight:normal;">
	    		User: <?php echo h($user['User']['username']); ?>
	    	</span>
	    </legend>

			<table width="100%">
			<tr>
				<td><strong>Username</strong></td>
				<td><?php echo h($user['User']['username']); ?></td>
			</tr>
			<tr>
				<td><strong>Password</strong></td>
				<td><?php echo h($user['User']['password']); ?></td>
			</tr>
			<tr>
				<td><strong>Role</strong></td>
				<td><?php echo h($user['User']['role']); ?></td>
			</tr>
			<tr>
				<td>Created On</td>
				<td><?php echo h($user['User']['created']); ?></td>
			</tr>
			</table>

			<?php echo $this->Html->link(
      	'Back',
        	array('controller' => 'users', 'action' => 'index'), array('class' => 'label button tiny round Secondary')); 
      ?>

			</fieldset>	

		</div>
	</div>
</div>