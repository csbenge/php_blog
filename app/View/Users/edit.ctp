<!-- File: /app/View/Users/edit.ctp -->

<div class="row">
	<div class="row">
		<div class="large-12 columns">
			<fieldset>
		    <legend>
		    	<span style="font-size: 1.75em;font-weight:normal;">
		    		<?php echo __('Edit User'); ?>
		    	</span>
		    </legend>
		    <div class="panel">
				<?php
					echo $this->Form->create('User');
					echo $this->Form->input('username');
					echo $this->Form->input('password');
		    	echo $this->Form->input('role', array(
		      	'options' => array('admin' => 'Admin', 'author' => 'Author')
			    ));
			    echo $this->Form->button('Add User', array('class' => 'label button tiny round'));
	        echo $this->Form->end();
			  ?>
			  <?php echo $this->Html->link(
	      	'Cancel',
	        	array('controller' => 'users', 'action' => 'index'), array('class' => 'label button tiny round Secondary')); 
	      ?>
				</div>
			</fieldset>	
		</div>
	</div>
</div>	