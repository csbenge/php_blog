<!-- File: /app/View/Users/index.ctp -->

<?php if ($users): ?>
<div class="row">
  <div class="row">
    <div class="large-6 columns">
      <h2>iBlog Users</h2>
    </div>  
    <div class="large-6 columns" style="text-align: right;vertical-align: center;">
      <br/>
      <?php echo $this->Html->link('Add User',
        array('controller' => 'users', 'action' => 'add'), array('class' => 'label button tiny round')); 
      ?>
   </div>
  </div>

  <div class="row">
    <div class="large-12 columns">

      <table width="100%">
        <thead>
          <tr>
            <th width="5%">ID</th>
            <th width="45%">Username</th>
           
            <th width="10%">Role</th>
            <th width="15%">Created</th>
            <th width="20%" style="text-align: center;">Actions</th>
          </tr>
        </thead>

        <!-- Here's where we loop through our $posts array, printing out post info -->

        <?php foreach ($users as $user): ?>
          <tr>
            <td>
              <?php echo $user['User']['id']; ?>
            </td>
            <td>
              <?php
                echo $this->Html->link(
                  $user['User']['username'], array('action' => 'view', $user['User']['id'])
                );
              ?>
            </td>
            
            <td>
              <?php echo $user['User']['role']; ?>
            </td>
            <td nowrap>
              <?php echo $user['User']['created']; ?>
            </td>
            <td nowrap style="text-align: center;">
              <?php
                echo $this->Html->link('View',
                  array('action' => 'view', $user['User']['id']),
                  array('class' => 'label button tiny round')
                );
              ?>
              <?php
                echo $this->Html->link('Edit',
                  array('action' => 'edit', $user['User']['id']),
                  array('class' => 'label button tiny round')
                );
              ?>
              <?php
                echo $this->Form->postLink('Delete',
                  array('action' => 'delete', $user['User']['id']),
                  array('class' => 'label button tiny round','confirm' => 'Are you sure?')
                );
              ?>
            </td>
          </tr>
        <?php endforeach; ?>

      </table>
    </div>
  </div>

</div>

<?php endif ?>