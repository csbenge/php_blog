<!-- app/View/Users/login.ctp -->

<div class="users form">
	<?php echo $this->Session->flash('auth'); ?>
	<?php echo $this->Form->create('User'); ?>
    <fieldset>
      <legend>
        <span style="font-size: 1.75em;font-weight:normal;">
          <?php echo __('Please Login'); ?>
        </span>
      </legend>
      <div class="panel">
        <?php 
          echo $this->Form->input('username');
  	      echo $this->Form->input('password');
          echo $this->Form->button('Login', array('class' => 'label button tiny round'));
          echo $this->Form->end();
  	    ?>
      </div>
    </fieldset>
</div>