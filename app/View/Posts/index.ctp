<!-- File: /app/View/Posts/index.ctp -->

<div class="row">
  <div class="row">
    <div class="large-6 columns">
      <h2>iBlog Posts</h2>
    </div>  
    <div class="large-6 columns" style="text-align: right;vertical-align: center;">
      <br/>
      <?php if (CakeSession::read('Auth.User.username')): ?>
        <?php echo $this->Html->link('Add Post',
          array('controller' => 'posts', 'action' => 'add'), array('class' => 'label button tiny round')); 
        ?>
      <?php endif ?>
   </div>
  </div>

  <div class="row">
    <div class="large-12 columns">

      <table width="100%">
        <thead>
          <tr>
            <th width="5%">ID</th>
            <th width="25%">Title</th>
            <th width="5%" style="text-align: center;">Comments</th>
            <th width="15%">Created By</th>
            <th nowrap width="12%">Created On</th>
            <th width="20%" style="text-align: center;">Actions</th>
          </tr>
        </thead>

        <!-- Here's where we loop through our $posts array, printing out post info -->

        <?php foreach ($posts as $post): ?>
          <tr>
            <td>
              <?php echo $post['Post']['id']; ?>
            </td>
            <td>
              <?php
                echo $this->Html->link(
                  $post['Post']['title'], array('action' => 'view', $post['Post']['id'])
                );
              ?>
            </td>
            <td style="text-align: center;">
              <?php echo $this->Post->getCommentCount($post['Post']['id']); ?>
            </td>
            <td>
              <?php echo $this->Post->getUserName($post['Post']['user_id']); ?>
            </td>
            <td nowrap>
              <?php echo $post['Post']['created']; ?>
            </td>
            <td nowrap style="text-align: center;">
              <?php
                echo $this->Html->link('View',
                  array('action' => 'view', $post['Post']['id']),
                  array('class' => 'label button tiny round')
                );
              ?>
              <?php if (CakeSession::read('Auth.User.id') === $post['Post']['user_id']): ?>
                <?php
                  echo $this->Html->link('Edit',
                    array('action' => 'edit', $post['Post']['id']),
                    array('class' => 'label button tiny round')
                  );
                ?>
              <?php endif ?>
              
              <?php if (CakeSession::read('Auth.User.id') === $post['Post']['user_id']): ?>
                <?php
                  echo $this->Form->postLink('Delete',
                    array('action' => 'delete', $post['Post']['id']),
                    array('class' => 'label button tiny round', 'confirm' => 'Are you sure?')
                  );
                ?>
                <?php
                  echo $this->Html->link('Move Up',
                    array('action' => 'moveup', $post['Post']['id']),
                    array('class' => 'label button tiny round')
                  );
                ?>

                <span data-tooltip aria-haspopup="true" class="has-tip" title="Move Up">
                  <i class="fi-arrow-up" style="font-size: 24px;"></i>
                </span>

              <?php endif ?>
            </td>
          </tr>
        <?php endforeach; ?>

      </table>
    </div>
  </div>

</div>