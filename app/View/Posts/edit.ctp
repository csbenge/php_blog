<!-- File: /app/View/Posts/edit.ctp -->

<div class="row">
	<div class="row">
		<div class="large-12 columns">
			<fieldset>
		    <legend>
		    	<span style="font-size: 1.75em;font-weight:normal;">
		    		<?php echo __('Edit Post'); ?>
		    	</span>
		    </legend>
		    <div class="panel">
				<?php
					echo $this->Form->create('Post');
					echo $this->Form->input('title');
					echo $this->Form->input('body', array('rows' => '3'));
					echo $this->Form->input('id', array('type' => 'hidden'));
					echo $this->Form->button('Update Post', array('class' => 'label button tiny round'));
					echo $this->Form->end();
				?>
				<?php echo $this->Html->link(
	        'Cancel',
		        array('controller' => 'posts', 'action' => 'index'), array('class' => 'label button tiny round Secondary')); 
		    ?>
				</div>
			</fieldset>	
		</div>
	</div>
</div>	