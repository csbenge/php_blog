<!-- File: /app/View/Posts/view.ctp -->

<div class="row">
	<div class="row">
		<div class="large-12 columns">
			<h2>
				<?php echo h($post['Post']['title']); ?>
			</h2>
			<p>
				<small>&nbsp;&nbsp;Created: <?php echo $post['Post']['created']; ?></small>
			</p>
			<hr>
		</div>
	</div>

	<div class="row">
		<div class="large-12 columns">
		  <div class="panel clearfix">
				<h3 class="subheader">
					<?php echo h($post['Post']['body']); ?>
				</h3>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="large-12 columns">
			<fieldset>
		    <legend><h4>Add Comment</h4></legend>
				<?php
					echo $this->Form->create('Comment',array('action' => 'add',
					  'url' => array($post['Post']['id'])));
					echo $this->Form->input('body', array('rows' => '3'));
					echo $this->Form->button('Add Comment', array('class' => 'label button tiny round'));
			    echo $this->Form->end();
				?>
			</fieldset>
		</div>
	</div>

	<div class="row">
		<div class="large-12 columns">
			<?php foreach ($post['Comment'] as $comment): ?>
			<fieldset>
				<legend>
				<h4><?php echo $this->Post->getUserName($comment['user_id']); ?> on <?php echo $comment['created']; ?></h4>
				</legend>
				<div class="panel clearfix">
					<div class="comment" style="margin-left:50px;">
			      <?php echo h($comment['body'])?>
			      <?php if ($this->Post->thisUserWroteComment($comment['id'])): ?>
		          <?php echo $this->Html->link('delete',
		    				array('controller' => 'comments', 'action' => 'delete', $comment['id'], $post['Post']['id'])); 
		      		?>
		      	<?php endif ?>
			    </div>
				
				</div>
			</fieldset>
			<?php endforeach; ?>
		</div>
	</div>

</div>