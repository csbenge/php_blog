<!-- File: /app/View/Posts/add.ctp -->

<div class="row">
	<div class="row">
		<div class="large-12 columns">
			<fieldset>
		    <legend>
		    	<span style="font-size: 1.75em;font-weight:normal;">
		    		<?php echo __('Add Post'); ?>
		    	</span>
		    </legend>
		    <div class="panel">
					<?php
						echo $this->Form->create('Post');
						echo $this->Form->input('title');
						echo $this->Form->input('body', array('rows' => '3'));
						echo $this->Form->button('Add Post', array('class' => 'label button tiny round'));
						echo $this->Form->end();
					?>
					<?php echo $this->Html->link(
		        'Cancel',
		        	array('controller' => 'posts', 'action' => 'index'), array('class' => 'label button tiny round Secondary')); 
		      ?>
				</div>
				</fieldset>	
			</div>
	</div>
</div>	