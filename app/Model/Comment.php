<?php

// File: /app/Model/Comment.php

class Comment extends AppModel {

  public $name      = 'Comment';
  public $belongsTo = array('Post', 'User');

  public $validate = array(
    'body' => array(
    	'rule' => 'notEmpty'
    )
  );

}