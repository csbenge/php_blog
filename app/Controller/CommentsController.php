<?php
App::uses('AppController', 'Controller');
/**
 * Comments Controller
 *
 * @property Comment $Comment
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CommentsController extends AppController {

  public $helpers = array('Html', 'Form', 'Session');
  public $components = array('Session');

  public function add($post_id = null) {
    if ($this->request->is('post')) {
      $this->Comment->set(array('post_id'=>$post_id));
      $this->Comment->set(array('user_id'=>CakeSession::read('Auth.User.id')));
      if ($this->Comment->save($this->request->data)) {
      	$this->Session->setFlash('Your comment has been added.');
      	//$this->redirect(array('action' => 'index'));
      	$this->redirect(array('controller' => 'posts', 'action' => 'view', $post_id));
      } else {
        $this->Session->setFlash('Unable to add your comment.');
      }
    }
	}

	public function delete($id, $post_id) {
		if ($this->Comment->delete($id)) {
	    $this->Session->setFlash('Your comment has been deleted.', 'default', array(), 'success');
	  	return $this->redirect(array('controller' => 'posts', 'action' => 'view', $post_id));
	  }
	  $this->Session->setFlash('Could not delete comment.', 'default', array(), 'alert');
	}

}
