<?php

// File: /app/Controller/PostsController.php

class PostsController extends AppController {
  public $helpers = array('Html', 'Form', 'Session', 'Post');
  public $components = array('Session');

  // Authentication/Authorization

  public function isAuthorized($user) {
    // All registered users can add posts
    if ($this->action === 'add') {
      return true;
    }

    // The owner of a post can edit and delete it
    if (in_array($this->action, array('edit', 'delete'))) {
      $postId = (int) $this->request->params['pass'][0];
      if ($this->Post->isOwnedBy($postId, $user['id'])) {
        return true;
      }
    }
    return parent::isAuthorized($user);
	}

  // Model Functions

  public function index() {
    $this->set('posts', $this->Post->find('all'));
  }

  public function view($id) {
    if (!$id) {
      throw new NotFoundException(__('Invalid post'));
    }

    $post = $this->Post->findById($id);
    if (!$post) {
      throw new NotFoundException(__('Invalid post'));
    }
    $this->set('post', $post);
  }

  public function add() {
    if ($this->request->is('post')) {
    	$this->request->data['Post']['user_id'] = $this->Auth->user('id');
			$this->Post->create();
      if ($this->Post->save($this->request->data)) {
        $this->Session->setFlash('Your post has been added.', 'default', array(), 'success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash('Unable to add your post.', 'default', array(), 'alert');
    }
  }

  public function edit($id = null) {
    if (!$id) {
      throw new NotFoundException(__('Invalid post'));
    }

    $post = $this->Post->findById($id);
    if (!$post) {
      throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
      $this->Post->id = $id;
      if ($this->Post->save($this->request->data)) {
        $this->Session->setFlash('Your post has been updated.', 'default', array(), 'success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash('Unable to update your post.', 'default', array(), 'alert');
    }

    if (!$this->request->data) {
      $this->request->data = $post;
    }
	}

	public function delete($id) {
    if ($this->request->is('get')) {
      throw new MethodNotAllowedException();
    }

    if ($this->Post->delete($id)) {
      //$this->Session->setFlash(__('The post with id: %s has been deleted.', h($id)));
      $this->Session->setFlash('Your post has been deleted.', 'default', array(), 'warning');
      return $this->redirect(array('action' => 'index'));
    }
	}

  public function moveup($id) {
    $this->Session->setFlash('Move Up!.', 'default', array(), 'success');
    return $this->redirect(array('action' => 'index'));
  }

}